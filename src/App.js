import { Link, Route, Routes } from "react-router-dom";
import './App.scss';
import Question from './components/Question/Question';
import Answer1 from './components/Answer1/Answer1';
import Answer2 from './components/Answer2/Answer2';
import Answer3 from './components/Answer3/Answer3';

function App() {
  return (
    <div>
      <header>
        <div className="header">Skedulo CX FE Assignments</div>
        <div className="nav">
          <div><Link to='/question'>Question</Link></div>
          <div><Link to='/answer-1'>Answer 1</Link></div>
          <div><Link to='/answer-2'>Answer 2</Link></div>
          <div><Link to='/answer-3'>Answer 3</Link></div>
        </div>
      </header>

      <div className="page">
        <Routes>
          <Route exact path="/" element={<Question />}/>
          <Route path="/question" element={<Question />}></Route>
          <Route path="/answer-1" element={<Answer1 />}></Route>
          <Route path="/answer-2" element={<Answer2 />}></Route>
          <Route path="/answer-3" element={<Answer3 />}></Route>
        </Routes>
      </div>

    </div>
  );
}

export default App;
