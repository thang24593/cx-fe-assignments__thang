import React from "react";
import { shallow } from "enzyme";
import App from "./App";

it("renders without crashing", () => {
  shallow(<App />);
});

it("renders App header", () => {
  const wrapper = shallow(<App />);
  const header = "Skedulo CX FE Assignments";
  expect(wrapper.contains(header)).toEqual(true);
});