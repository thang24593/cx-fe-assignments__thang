import React from 'react';
import Markdown from 'markdown-to-jsx';
import { useEffect, useState } from "react";
import { Container, ButtonGroup, Button } from 'react-bootstrap';

const Question = () => {
  const [questionNumber, setQuestionNumber] = useState(1)
  const [question, setQuestion] = useState()

  useEffect(
    () => {
      if (!!questionNumber) {
        import(`../../question${questionNumber}/INSTRUCTIONS.md`)
          .then(response => fetch(response.default))
          .then((response) => response.text())
          .then(instructionData => setQuestion(instructionData))
      }
    },
    [questionNumber],
  )

  return (<Container fluid>
    <ButtonGroup aria-label="Question">
      <Button className={questionNumber === 1 ? 'active' : ''} onClick={() => setQuestionNumber(1)} variant="secondary">Question 1</Button>
      <Button className={questionNumber === 2 ? 'active' : ''} onClick={() => setQuestionNumber(2)} variant="secondary">Question 2</Button>
      <Button className={questionNumber === 3 ? 'active' : ''} onClick={() => setQuestionNumber(3)} variant="secondary">Question 3</Button>
    </ButtonGroup>
    
    {question && <Markdown>{question}</Markdown>}
  </Container>);
};

export default Question;
