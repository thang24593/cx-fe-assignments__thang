import { shallow } from "enzyme";
import React from "react";
import Answer2, { unionOverlappingItems } from "./Answer2";
import { render, getByTestId, fireEvent } from '@testing-library/react';

describe('Answer2', () => {
  it("renders Answer2 heading", () => {
    const wrapper = shallow(<Answer2 />);
    const heading = "Answer2 Component";
    expect(wrapper.contains(heading)).toEqual(true);
  });

  test('func works with default input data', () => {
    const data = [
      { startPx: 50, endPx: 90 },
      { startPx: 10, endPx: 30 },
      { startPx: 20, endPx: 40 }
    ];
    const outputData = [
      { startPx: 10, endPx: 40 },
      { startPx: 50, endPx: 90 }
    ];
    expect(unionOverlappingItems(data)).toEqual(outputData);
  });

  test('func works with mock input data', () => {
    const data = [
      { startPx: 50, endPx: 90 },
      { startPx: 10, endPx: 30 }, // case overlap
      { startPx: 20, endPx: 40 },
      { startPx: 120, endPx: 160 }, // case inside 
      { startPx: 130, endPx: 140 }
    ];
    const outputData = [
      { startPx: 10, endPx: 40 },
      { startPx: 50, endPx: 90 },
      { startPx: 120, endPx: 160 }
    ];
    expect(unionOverlappingItems(data)).toEqual(outputData);
  });
});
