import React from 'react';
import { useState } from "react";
import { Container, Button, Form } from 'react-bootstrap';

const data = [
  { startPx: 50, endPx: 90 },
  { startPx: 10, endPx: 30 },
  { startPx: 20, endPx: 40 }
];

export function unionOverlappingItems(data) {
  // sort data by startPx value
  const sortedData = data.sort((a, b) => a.startPx - b.startPx);
  const res = sortedData.reduce((arr, current) => {
    const lastImemInArr = arr[arr.length - 1];
    // check condition when item 1 & 2 are overlapping
    if (lastImemInArr &&
      lastImemInArr.startPx <= current.startPx &&
      current.startPx <= lastImemInArr.endPx) {
      // check condition when endPx of item 2 is larger than endPx of item 1
      if (lastImemInArr.endPx < current.endPx) {
        lastImemInArr.endPx = current.endPx;
      }
      // return array with modified value of the last item
      return arr;
    }
    return arr.concat(current);
  }, []);
  return res;
};

const Answer2 = () => {
  const [results, setResults] = useState('')

  return (<Container fluid>
    <h1>Answer2 Component</h1>

    <Form>
      <Form.Group className="mb-3" controlId="answer2.ControlTextarea1">
        <Form.Label>Input</Form.Label>
        <Form.Control as="textarea" rows={8} defaultValue={JSON.stringify(data, undefined, 4)} disabled />
      </Form.Group>
    </Form>
    <Button data-testid="btn-run" className="mb-3" variant="primary" onClick={() => setResults(unionOverlappingItems(data))}>Run</Button>{' '}
    <Form>
      <Form.Group className="mb-3" controlId="answer2.ControlTextarea2">
        <Form.Label>Output</Form.Label>
        <Form.Control data-testid="textarea-output" as="textarea" rows={8} value={JSON.stringify(results, undefined, 4)} disabled />
      </Form.Group>
    </Form>
  </Container>);
};

export default Answer2;
