import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Container, Form, Row, Col, Button } from 'react-bootstrap';

const Answer1 = () => {
  const [loading, setLoading] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  // live search field when typing in input field
  useEffect(() => {
    const fetchData = async () => {
      await axios.get(`https://api.github.com/search/users?q=${encodeURIComponent(searchTerm)}`)
        .then((res) => {
          setSearchResults(res.data.items);
        })
        .catch((error) => {
          if (error.response) {
            alert(error.response.data.message);
          } else if (error.request) {
            alert(error.request);
          } else {
            alert('Error', error.message);
          }
          console.log(error);
        })
        .then(() => {
          setLoading(false);
        });
    };

    // search only when typing at least 3 chars
    if (searchTerm.length >= 3) {
      setLoading(true);
      fetchData();
    }

    // clear the results if input field is empty
    if (searchTerm.length === 0) {
      setSearchResults([]);
    }
  }, [searchTerm]);

  const handleChange = event => {
    setSearchTerm(event.target.value);
  };

  return (<Container fluid>
    <h1>Answer1 Component</h1>

    <Form>
      <Form.Group className="mb-3" controlId="answer1.ControlTextarea1">
        <Form.Label>Search Github user</Form.Label>
        <Form.Control as="input" value={searchTerm} onChange={handleChange} />
      </Form.Group>
    </Form>

    <Button className="mb-5" size="sm" variant="outline-primary" onClick={() => setSearchTerm('')}>Clear</Button>{' '}

    {loading && <div>
      <img src="https://i.pinimg.com/originals/49/23/29/492329d446c422b0483677d0318ab4fa.gif" alt="Loading..." width="300" height="201" />
    </div>}
    {!loading && <div>
      {(searchResults.length === 0 && searchTerm !== '') && <div>
        <p>There is no data to display.</p>
      </div>}
      {searchResults.length > 0 && searchResults.map((item) => (
        <Row className="mb-3" key={item.id}>
          <Col xs="auto">
            <img src={item.avatar_url} alt="avatar" width="34" height="34" />
          </Col>
          <Col>
            <p className="mb-1">Name: {item.login}</p>
            <p>Repo: <a href={item.html_url} target="_blank" rel="noreferrer">{item.html_url}</a></p>
          </Col>
        </Row>
      ))}
    </div>}
  </Container>);
};

export default Answer1;
