/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import GetUsers from './get-users';
import { useEffect, useState } from "react";
import { Container, Card, Row, Col } from 'react-bootstrap';
import axios from 'axios';

const Answer3 = () => {
  const { data, loading } = GetUsers();
  const [selectedItem, setSelectedItem] = useState('')
  const [details, setDetails] = useState({})
  const [repos, setRepos] = useState([])

  useEffect(() => {
    if (data && data.length > 0) {
      setSelectedItem(data[0].login);
    }
  }, [data])

  useEffect(() => {
    const fetchData = async () => {
      if (selectedItem) {
        setDetails({});
        await axios.get(`https://api.github.com/users/${selectedItem}`)
          .then((res) => {
            setDetails(res.data);
          })
          .catch((error) => {
            if (error.response) {
              alert(error.response.data.message);
            } else if (error.request) {
              alert(error.request);
            } else {
              alert('Error', error.message);
            }
            console.log(error);
          });
      }
    };
    fetchData();
  }, [selectedItem]);

  useEffect(() => {
    const fetchData = async () => {
      if (selectedItem) {
        setRepos([]);
        await axios.get(`https://api.github.com/users/${selectedItem}/repos?page=0&per_page=100`)
          .then((res) => {
            setRepos(res.data);
          })
          .catch((error) => {
            if (error.response) {
              alert(error.response.data.message);
            } else if (error.request) {
              alert(error.request);
            } else {
              alert('Error', error.message);
            }
            console.log(error);
          });
      }
    };
    fetchData();
  }, [selectedItem]);

  return (<Container fluid className="answer-3">
    <h1>Answer3 Component</h1>

    {loading && <div>
      <img src="https://i.pinimg.com/originals/49/23/29/492329d446c422b0483677d0318ab4fa.gif" alt="Loading..." width="300" height="201" />
    </div>}
    {!loading && <Row>
      <Col xs={4} md={3} xl={2} className="column sidebar">
        {data.map((item) => (
          <a href="#" className="d-block" key={item.id} onClick={() => { setSelectedItem(item.login) }}>
            <Card
              bg={selectedItem === item.login ? "primary" : "light"}
              text={selectedItem === item.login ? "white" : "dark"}>
              <Card.Body>
                <Card.Text>
                  {item.login}
                </Card.Text>
              </Card.Body>
            </Card>
          </a>
        ))}
      </Col>

      <Col className="column main-content">
        <Row>
          <Col>
            <h2>User information</h2>
            {Object.entries(details).length !== 0 && <div>
              <img className="mb-3" src={details.avatar_url} alt="avatar" width="50" height="50" />
              <p>Name: {details.name}</p>
              <p>Company: {details.company}</p>
              <p>GIT: <a href={details.html_url} target="_blank" rel="noreferrer"> {details.html_url} </a>
              </p>
              <p>Repositories: {details.public_repos}</p>
              <p>Followers: {details.followers}</p>
              <p>Following: {details.following}</p>
            </div>}
          </Col>

          <Col>
            <h2>Repositories</h2>
            {Object.entries(repos).length !== 0 && <div>
              {repos.map((repo) => (
                <p key={repo.id}>
                  <a href={repo.html_url} className="text-break" target="_blank" rel="noreferrer">
                    {repo.html_url}
                  </a>
                </p>
              ))}
            </div>}
          </Col>
        </Row>
      </Col>
    </Row>}

  </Container>);
};

export default Answer3;
