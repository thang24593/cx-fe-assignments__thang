import { useState, useEffect } from 'react';
import axios from 'axios';

const GetUsers = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      await axios.get('https://api.github.com/users?page=0&per_page=100')
        .then((res) => {
          setData(res.data);
        })
        .catch((error) => {
          if (error.response) {
            alert(error.response.data.message);
          } else if (error.request) {
            alert(error.request);
          } else {
            alert('Error', error.message);
          }
          console.log(error);
        })
        .then(() => {
          setLoading(false);
        });
    }
    fetchData();
  }, []);

  return { data, loading };
};

export default GetUsers;