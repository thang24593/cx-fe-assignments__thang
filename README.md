# Skedulo CX FE Assignments

## Installation Instructions

- Install: `yarn install`

## Running the App

- Run: `yarn start`

## Overview

Hi! Welcome to the Skedulo CX frontend assignment project. You will see we have 3 questions and each of their instructions are located in the local INSTRUCTIONS.md. Please complete these exercises then save to a private github repository. Once you're ready reach out to your hiring manager and they will share some github names to add as collaborators who will review your submission.

Once you have completed all exercises it would be great to see a readme that contains some of your thinking while working through the test, constraints you worked within, some decisions and compromises you needed to make, and anything else you think would be interesting for the reviewers to know.

- Question 1 - React input example
- Question 2 - Problem solving
- Question 3 - Responsive CSS design

Good luck! And if you have any questions, don't hesitate to contact your hiring manager.

## What we're looking for ?

The purpose of the test is not only to make the solution workable, but also it has very well documented and structured code, as it serves as an example of how serious the developer is to push the team to write quality code.

- Need to code best practices and code design.
- **_You should use only plain HTML, CSS and Javascript, NO 3rd party libraries, only Bootstrap, React & Jest are allowed._**
- Main code function is one big function, it needs to be split to be more readable and maintainable.
- A readme that discusses your approach, anything that you think would be valuable for the reviewer to know


## Note from Thang
About my answers for this assignment, maybe It isn't good or we can find out the better solution because my strong skill is Angular. It's too long time to work with the latest version of React. So, if there is anything you think we can enhance, pls share it to me. Thank you!

There are some issues/problems which I'd like to note below due to not having enough time to complete:

Q1:
- It still has issue "API rate limit exceeded" when request API too many times. Currently I just let it show error for users.
- Should prevent input special chars and set max length for the input search field.

Q2:
- The current output is diff with the example (ordering of the list of items) because I decide to sort by ASC for this solution. If we must get ordering of the output exactly the same with input data, I think solution is using linear search with 2 loops in the array. But it's not good for the performance.

Q3:
- I just use github API with params per and per_page to get the list of users and repos. There are many ways to enhance it such as infinite scroll or virtual scroll